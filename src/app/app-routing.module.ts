import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RatingsComponent } from './ratings/ratings.component';
import { RewardsComponent } from './rewards/rewards.component';
import { PrizeComponent } from './prizes/prizes.component';
import { LogoutComponent } from './logout/logout.component';

const routes: Routes = [
	{path: '', redirectTo: '/login', pathMatch: 'full'},
	{path: 'login', component: LoginComponent},
	{path: 'ratings', component: RatingsComponent},
	{path: 'rewards', component: RewardsComponent},
	{path: 'prizes', component: PrizeComponent},
	{path: 'logout', component: LogoutComponent},
	{path: '**', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)], 	
  exports: [RouterModule]
})
export class AppRoutingModule { }
