
export class Session{
	
	constructor(){}

	storeSession(user: {user_token: null, player_id: null, authenticated: null} ):void{ 
		localStorage.setItem('user_token', user.user_token);
		localStorage.setItem('player_id', user.player_id);
		localStorage.setItem('authenticated', user.authenticated);
	}


	killSession():void{
		localStorage.removeItem('user_token');
		localStorage.removeItem('player_id');
		localStorage.removeItem('authenticated');
	}

}
