import { TestBed } from '@angular/core/testing';
import { ApolloModule } from 'apollo-angular';


import { LoginService } from './login.service';

describe('LoginService', () => {
  beforeEach(() => TestBed.configureTestingModule({
  	declarations: [ ],
    imports: [ ApolloModule]
  }));

  it('should be created', () => {
    const service: LoginService = TestBed.get(LoginService);
    expect(service).toBeTruthy();
  });
});
