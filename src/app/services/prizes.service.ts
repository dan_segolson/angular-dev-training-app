import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';


export interface Prize {
	prizeId: string;
	title: string;
	description: string;	
	prizeInfoType: string;		
}

export interface Response{
	allPrizes: Prize[];
}

@Injectable({
  providedIn: 'root'
})

export class PrizeService extends Query<Response> {

	document = gql`
			  {
			    rewards {
				    prizes{
					    prizeId
					    title
					    description  
					    prizeInfoType
				    }
				  }
			  }
			`;
		
}