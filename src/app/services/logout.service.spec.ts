import { TestBed } from '@angular/core/testing';
import { ApolloModule } from 'apollo-angular';

import { LogoutService } from './logout.service';

describe('LogoutService', () => {
  beforeEach(() => TestBed.configureTestingModule({
  	declarations: [ ],
    imports: [ ApolloModule]
  }));

  it('should be created', () => {
    const service: LogoutService = TestBed.get(LogoutService);
    expect(service).toBeTruthy();
  });
});
