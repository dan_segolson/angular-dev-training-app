import { TestBed } from '@angular/core/testing';
import { ApolloModule } from 'apollo-angular';

import { RewardService } from './rewards.service';

describe('RewardsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
  	declarations: [ ],
    imports: [ ApolloModule]
  }));

  it('should be created', () => {
    const service: RewardService = TestBed.get(RewardService);
    expect(service).toBeTruthy();
  });
});
