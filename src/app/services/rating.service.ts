import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';


export interface Rating {
	PlayerId: string;
	Points: string;
	Username: string;		
}

export interface Response{
	allRatings: Rating[];
}

@Injectable({
  providedIn: 'root'
})

export class RatingService extends Query<Response> {

	document = gql`
			  query playerRatingsQuery($app_token: String!, $player_id: String!, $user_token: String!, $char_id: String, $lb_type: String!, $limit: String!, $year: String!, $month: String!) {
			    stage_player_ratings(app_token: $app_token, player_id: $player_id, user_token: $user_token, char_id: $char_id, lb_type: $lb_type, limit: $limit, year: $year, month: $month) {
			        status
				    getPlayerRatings {
				      Points
				      Username
				      PlayerId
				    }
			    }
			  }
			`;
		
}
