import { TestBed } from '@angular/core/testing';
import { ApolloModule } from 'apollo-angular';

import { PrizeService } from './prizes.service';

describe('PrizesService', () => {
  beforeEach(() => TestBed.configureTestingModule({
  	declarations: [ ],
    imports: [ ApolloModule]
  }));

  it('should be created', () => {
    const service: PrizeService = TestBed.get(PrizeService);
    expect(service).toBeTruthy();
  });
});
