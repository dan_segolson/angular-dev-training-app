import { TestBed } from '@angular/core/testing';
import { ApolloModule } from 'apollo-angular';

import { RatingService } from './rating.service';

describe('RatingService', () => {
  beforeEach(() => TestBed.configureTestingModule({
  	declarations: [ ],
    imports: [ ApolloModule]
  }));

  it('should be created', () => {
    const service: RatingService = TestBed.get(RatingService);
    expect(service).toBeTruthy();
  });
});
