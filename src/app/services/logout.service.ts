import { Injectable } from '@angular/core';
import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';


@Injectable({
  providedIn: 'root'
})


export class LogoutService extends Mutation<Response> {

	document = gql`
			  mutation LogoutMutation($app_token: String!, $player_id: String!, $user_token: String!) {
			    stage_logout(app_token: $app_token, player_id: $player_id, user_token: $user_token) {
			        iLoginStatus
			        iPlayerId
			        error
			    }
			  }
			`;
		
}

