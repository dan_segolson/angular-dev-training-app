import { Injectable } from '@angular/core';
import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';


@Injectable({
  providedIn: 'root'
})


export class LoginService extends Mutation {

	document = gql`
			   mutation LoginMutation($app_token: String!, $username: String!, $password: String!) {
			    stage_login(app_token: $app_token, username: $username, password: $password) {
			    		iLoginStatus
				    	iPlayerId
				    	sUserName
				      	sLoginToken	
				      	error
			    }
			  }
			`;
		
}
