import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';


export interface Reward {
	RewardId: string;
	RewardType: string;
	RewardName: string;
	RewardDescription: string;
	RewardsIdentifier: string;
	ItemId: string;		
	RewardAmount: string;		
	CharId: string;		
}

export interface Response{
	allRewards: Reward[];
}

@Injectable({
  providedIn: 'root'
})

export class RewardService extends Query<Response> {

	document = gql`
			  query ingameRewards($app_token: String!, $player_id: String!, $user_token: String!) {
			    stage_game_rewards(app_token: $app_token, player_id: $player_id, user_token: $user_token) {
					RewardId
					RewardType
					RewardName
					RewardDescription
					RewardsIdentifier
					ItemId
					RewardAmount
					CharId    
			    }
			  }
			`;
		
}