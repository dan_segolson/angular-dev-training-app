import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Config } from '../conf/config';
import { LoginService } from '../services/login.service'
import { Session } from '../utilities/utilities'


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html', 
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  	@Input()

  	user: object;
  	session: any;
	login_data: any;
	error: any;
	status: any;

	app_token: string;
	username: string;
	password: string;

	player_id: string;
	user_token: string;
	authenticated: boolean;

	model = {username:'', password: '', message: '', alert_class: ''}


	constructor(private loginService: LoginService,  private router: Router) { }

	ngOnInit() {
		this.authenticated = false;
		this.session = new Session();
	}

	loginUser() :void {
		this.setLoginParams();
		this.loginService.mutate({
				app_token: this.app_token, 
				username: this.username,
				password: this.password
			}).subscribe(result => {this.setLoginData(result.data.stage_login)});
	}

	setLoginData(result: {iLoginStatus: null, iPlayerId: null, sLoginToken: null}):void{
		//console.log(result);
		if((result.iLoginStatus) && (result.iLoginStatus === 'True')){
			this.player_id = result.iPlayerId;
			this.user_token = result.sLoginToken;
			this.authenticated = true;	
			this.model.alert_class = 'text-success';
			this.model.message = 'Correct you are, let in you will be'
			this.user = {player_id: this.player_id,user_token: this.user_token,authenticated: this.authenticated}
			this.session.storeSession(this.user);
			this.router.navigate([Config.LOGIN_REDIR])
		}else{
			//console.log('Meet General Error...');
			this.player_id = '';
			this.user_token = '';
			this.authenticated = false;	
			this.model.alert_class = 'text-danger';
			this.model.message = 'Wrongs there be, correct them you should.';
			this.session.killSession();
		}
		//console.log('id: '+ this.player_id +' token: '+ this.user_token + ' auth: '+ this.authenticated);
		

	}

	 /**
	 * Get the token from const, 
	 * player_id & user token from data saved on player login (pref not local storage)
	 */
	setLoginParams(): void {
		this.app_token = Config.APP_TOKEN;
		this.username = this.model.username;
		this.password = this.model.password;
	}

}
