import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LoginComponent } from './login.component';

import { FormsModule } from '@angular/forms';
import { ApolloModule } from 'apollo-angular';
import { AppRoutingModule } from '../app-routing.module';

import { NavigationComponent } from '../navigation/shared-nav/navigation.component';
import { RatingsComponent } from '../ratings/ratings.component';
import { RewardsComponent } from '../rewards/rewards.component';
import { PrizeComponent } from '../prizes/prizes.component';
import { LogoutComponent } from '../logout/logout.component';


describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent, NavigationComponent, RatingsComponent, RewardsComponent, PrizeComponent, LogoutComponent],
      imports: [ FormsModule, ApolloModule, AppRoutingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
