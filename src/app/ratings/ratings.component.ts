import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { Config } from '../conf/config';
import { Session } from '../utilities/utilities';
import { Rating, RatingService } from '../services/rating.service'


@Component({
  selector: 'app-ratings',
  templateUrl: './ratings.component.html',
  styleUrls: ['./ratings.component.css']
})


export class RatingsComponent implements OnInit {

	authenticated: boolean;
	session: any;

	resp_data: any;
	ratings: Observable<Rating[]>;
	loading = true;
	error: any;
	status: any;

	app_token: string;
	player_id: string;
	user_token: string;
	char_id: string;
	lb_type: string;
	limit: string;
	year: string;
	month: string;



	constructor(private ratingService: RatingService, private router: Router) { }
	
	ngOnInit() {
		this.session = new Session();
		this.authenticated = (/true/i).test(localStorage.getItem('authenticated')); //the value from local storage is a string, I want tit as a bool

		if(this.authenticated){
			this.serviceGetRatings();
		}else{
			this.session.killSession();
			this.router.navigate(['/login']);
		}
	}



	serviceGetRatings(){
		this.setQueryParams();

		this.resp_data = this.ratingService.watch({
				app_token: this.app_token, 
				player_id: this.player_id,
				user_token: this.user_token,
				char_id: this.char_id, 
				lb_type: this.lb_type,
				limit: this.limit,
				year: this.year,
				month: this.month
			}).valueChanges;

		//this.resp_data.subscribe(result => console.log(result));
		this.resp_data.subscribe(result => {this.loading = result.loading});
		this.resp_data.subscribe(result => {this.error = result.error});
		this.resp_data.subscribe(result => {this.showData(result.data.stage_player_ratings)});
	}

	showData(result: {status: null, getPlayerRatings:null} ):void {
		if((result.status === 'invalid token') || (result.status === 'expired')|| (result.status === 'Expired')){
			this.status = result.status;
			this.authenticated = false;
			this.session.killSession();
			this.router.navigate(['/login'])
		}else{
			this.ratings = result.getPlayerRatings;	
		}
	}

	/**
	 * Get the token from const, 
	 * player_id & user token from data saved on player login (pref not local storage)
	 * lb_type  is fixed here
	 * limit
	 * (char_id, year, month not used, but needed in signature call) 
	 */
	setQueryParams(){
		this.app_token = Config.APP_TOKEN; 
		this.player_id = localStorage.getItem('player_id');
		this.user_token = localStorage.getItem('user_token');
		this.char_id = '1';
		this.lb_type = 'getPlayerRatings';
		this.limit = '50';
		this.year = '2018';
		this.month = '12';
		//console.log(this)
	}

}
