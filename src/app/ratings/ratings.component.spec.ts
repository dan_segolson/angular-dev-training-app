import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RatingsComponent } from './ratings.component';

import { FormsModule } from '@angular/forms';
import { ApolloModule } from 'apollo-angular';
import { AppRoutingModule } from '../app-routing.module';

import { NavigationComponent } from '../navigation/shared-nav/navigation.component';
import { RewardsComponent } from '../rewards/rewards.component';
import { PrizeComponent } from '../prizes/prizes.component';
import { LoginComponent } from '../login/login.component';
import { LogoutComponent } from '../logout/logout.component';





describe('RatingsComponent', () => {
  let component: RatingsComponent;
  let fixture: ComponentFixture<RatingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent, NavigationComponent, RatingsComponent, RewardsComponent, PrizeComponent, LogoutComponent],
      imports: [ FormsModule, ApolloModule, AppRoutingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RatingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
