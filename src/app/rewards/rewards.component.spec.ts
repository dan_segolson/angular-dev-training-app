import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RewardsComponent } from './rewards.component';

import { FormsModule } from '@angular/forms';
import { ApolloModule } from 'apollo-angular';
import { AppRoutingModule } from '../app-routing.module';

import { NavigationComponent } from '../navigation/shared-nav/navigation.component';
import { PrizeComponent } from '../prizes/prizes.component';
import { RatingsComponent } from '../ratings/ratings.component';
import { LoginComponent } from '../login/login.component';
import { LogoutComponent } from '../logout/logout.component';

describe('RewardsComponent', () => {
  let component: RewardsComponent;
  let fixture: ComponentFixture<RewardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent, NavigationComponent, RatingsComponent, RewardsComponent, PrizeComponent, LogoutComponent],
      imports: [ FormsModule, ApolloModule, AppRoutingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RewardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
