import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { Config } from '../conf/config';
import { Session } from '../utilities/utilities';
import { Reward, RewardService } from '../services/rewards.service'

@Component({
	selector: 'app-rewards',
	templateUrl: './rewards.component.html',
	styleUrls: ['./rewards.component.css']
})

export class RewardsComponent implements OnInit {

	authenticated: boolean;
	session: any;

	resp_data: any;
	rewards: any; //Observable<Reward[]>;
	loading = true;
	error: any;
	status: any;

	app_token: string;
	player_id: string;
	user_token: string;

	constructor(private rewardService: RewardService, private router: Router) { }

	ngOnInit() {
		this.session = new Session();
		this.authenticated = (/true/i).test(localStorage.getItem('authenticated')); //the value from local storage is a string, I want tit as a bool

		if(this.authenticated){
			this.setQueryParams();
			this.serviceGetRewards();	
		}else{
			this.session.killSession();
			this.router.navigate(['/login']);
		}
		
	}

	serviceGetRewards(){

		this.resp_data = this.rewardService.watch({
				app_token: this.app_token, 
				player_id: this.player_id,
				user_token: this.user_token,
			}).valueChanges;

		//this.resp_data.subscribe(result => console.log(result));
		this.resp_data.subscribe(result => {this.loading = result.loading});
		this.resp_data.subscribe(result => {this.error = result.error});

		this.resp_data.subscribe(result => {this.status = result.data.stage_game_rewards.status});
		this.resp_data.subscribe(result => {this.showData(result.data.stage_game_rewards)});

	}

	showData(result: {status: null} ):void {
		if((result.status === 'invalid token') || (result.status === 'expired')|| (result.status === 'Expired')){
			this.status = result.status;
			this.authenticated = false;
			this.session.killSession();
			this.router.navigate(['/login'])
		}else{
			this.rewards = result;	
		}
	}


  /**
	 * Get the token from const, 
	 * player_id & user token from data saved on player login (pref not local storage)
	 */
	setQueryParams(){
		this.app_token = Config.APP_TOKEN;
		this.player_id = localStorage.getItem('player_id');
		this.user_token = localStorage.getItem('user_token');
	}

}
