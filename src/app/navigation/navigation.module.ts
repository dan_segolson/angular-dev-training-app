import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from '../app-routing.module';
import { NavigationComponent } from './shared-nav/navigation.component';

@NgModule({
  declarations: [
  	NavigationComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule
  ],
  exports: [
    NavigationComponent
  ]
})
export class NavigationModule { }
