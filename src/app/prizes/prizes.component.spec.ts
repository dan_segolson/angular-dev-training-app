import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PrizeComponent } from './prizes.component';

import { FormsModule } from '@angular/forms';
import { ApolloModule } from 'apollo-angular';
import { AppRoutingModule } from '../app-routing.module';

import { NavigationComponent } from '../navigation/shared-nav/navigation.component';
import { RatingsComponent } from '../ratings/ratings.component';
import { RewardsComponent } from '../rewards/rewards.component';
import { LoginComponent } from '../login/login.component';
import { LogoutComponent } from '../logout/logout.component';


describe('PrizesComponent', () => {
  let component: PrizeComponent;
  let fixture: ComponentFixture<PrizeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent, NavigationComponent, RatingsComponent, RewardsComponent, PrizeComponent, LogoutComponent ],
      imports: [ FormsModule, ApolloModule, AppRoutingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
