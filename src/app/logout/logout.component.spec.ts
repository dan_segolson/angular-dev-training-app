import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LogoutComponent } from './logout.component';

import { FormsModule } from '@angular/forms';
import { ApolloModule } from 'apollo-angular';
import { AppRoutingModule } from '../app-routing.module';


import { LoginComponent } from '../login/login.component';
import { NavigationComponent } from '../navigation/shared-nav/navigation.component';
import { RatingsComponent } from '../ratings/ratings.component';
import { RewardsComponent } from '../rewards/rewards.component';
import { PrizeComponent } from '../prizes/prizes.component';




describe('LogoutComponent', () => {
  let component: LogoutComponent;
  let fixture: ComponentFixture<LogoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogoutComponent, LoginComponent, NavigationComponent, RatingsComponent, RewardsComponent, PrizeComponent ],
      imports: [ FormsModule, ApolloModule, AppRoutingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
