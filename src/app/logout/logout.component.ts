import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Config } from '../conf/config';
import { Session } from '../utilities/utilities';
import { LogoutService } from '../services/logout.service'

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent {

	@Input()

	authenticated: boolean
	session: any;

	resp_data:any;
	loading = true;
	error: any;
	status: any;

	app_token: string;
	player_id: string;
	user_token: string;

  constructor(private logoutService: LogoutService,  private router: Router) { }

  ngOnInit() {
  	this.authenticated = (/true/i).test(localStorage.getItem('authenticated')); 
  	this.session = new Session();
  }

  logoutUser() :void {
		this.setParams();
		this.logoutService.mutate({
				app_token: this.app_token, 
				player_id: this.player_id,
				user_token: this.user_token
			}).subscribe(result => {this.setLogout(result.data.stage_logout)});
	}

	setLogout(result: {error: null, iLoginStatus:null}):void{
		//console.log(result);
		if((result.iLoginStatus === 'Logged out') || (result.error === 'No such logged in user')){
			this.session.killSession();
			this.router.navigate(['/login']);
		}
	}

	setParams(){
		this.app_token = Config.APP_TOKEN;
		this.player_id = localStorage.getItem('player_id');
		this.user_token = localStorage.getItem('user_token');
	}

}
