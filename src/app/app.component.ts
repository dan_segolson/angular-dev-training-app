import { Component, OnInit } from '@angular/core';
//import { NavigationComponent } from './navigation/shared-nav/navigation.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'angular-training-app';
  
  constructor(){ }

  ngOnInit(){
  }

}
